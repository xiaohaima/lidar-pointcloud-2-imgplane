# Author: Xianghe Ma
# email: xiaohemaikoo@gmail.com
from numpy import genfromtxt
import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos
from PIL import Image

pointcloud_lidar = genfromtxt('pointcloud.csv', delimiter=',')

ptc_homo = np.ones((4, pointcloud_lidar.shape[1]))
ptc_homo[0:3, :] = pointcloud_lidar[0:3, :]

roll = -0.012
pitch = 0.02
yaw = 0
# rotation matrix on roll direction (x direction)
R_roll = np.array([[1, 0, 0],
                   [0, cos(roll), -sin(roll)],
                   [0, sin(roll), cos(roll)]])
# rotation matrix on pitch direction (y direction)
R_pitch = np.array([[cos(pitch), 0, sin(pitch)],
                    [0, 1, 0],
                    [-sin(pitch), 0, cos(pitch)]])
# rotation matrix on yaw direction (z direction)
R_yaw = np.array([[cos(yaw), -sin(yaw), 0],
                  [sin(yaw), cos(yaw), 0],
                  [0, 0, 1]])

# rotation matrix, diff multiply sequence has a little diffs on rotation matrix
R = np.dot(R_roll, R_pitch).dot(R_yaw)
# R = np.dot(R_yaw, R_pitch).dot(R_roll)

# translation matrix
T = np.array([1.95, 0, 1.29])

# point cloud project to image plane version 1
def pc_to_pixels(ptc_homo, R, T):
    pt_pixels = []
    h = 960
    w = 1280
    # focal length
    f = 6.05e-3
    # Camera pixel dimensions in m
    pixel_dim = 3.75e-6
    # Camera pixels per m
    pixels_m = 1/pixel_dim
    a = f * pixels_m
    b = f * pixels_m

    # camera intrinsics matrix
    K = np.array([[a, 0, h/2],
                  [0, b, -w/2],
                  [0, 0, 1]])

    M_ = np.zeros((3, 4))
    M_[:, 0:3] = R
    M_[:, 3] = T

    # projection matrix
    M = np.dot(K, M_)

    # convert img point pixels position in homogeneous coordinates to img pixels position
    pt_img_homo = np.dot(M, ptc_homo)
    for i in range(pt_img_homo.shape[1]):
        if -1e-8 > pt_img_homo[2, i] or 1e-8 < pt_img_homo[2, i]:
            pt_pixels.append(pt_img_homo[0:2, i] / pt_img_homo[2, i])
        else:
            print("Warning: intensity index will change")

    return np.array(pt_pixels)


# point cloud project to image plane version 2
def pc_to_pixels_v2(ptc_homo, R, T):
    RT = np.array([[R[0, 0], R[0, 1], R[0, 2], T[0]],
                   [R[1, 0], R[1, 1], R[1, 2], T[1]],
                   [R[2, 0], R[2, 1], R[2, 2], T[2]],
                   [0, 0, 0, 1]])
    ptc_homo_camera_coordinate = np.dot(RT, ptc_homo)
    M = np.array([[6.05e-3, 0, 0, 0],
                  [0, 6.05e-3, 0, 0],
                  [0, 0, 1, 0]])
    pt_homo_img = np.dot(M, ptc_homo_camera_coordinate)

    # convert image homogeneous coordinates pt_homo_img to coordinates in image plane (img center is coordinates origin)
    pt_imgp = []
    for i in range(pt_homo_img.shape[1]):
        if -1e-8 > pt_homo_img[2, i] or 1e-8 < pt_homo_img[2, i]:
            pt_imgp.append(pt_homo_img[0:2, i] / pt_homo_img[2, i])
        else:
            print("Warning: intensity index will change")

    pt_imgp = np.array(pt_imgp).T

    # convert image coordinates from img center is coordinates origin to img corner origin
    pixel_dim_m = 3.75e-6
    h = 960
    w = 1280
    oh = (h / 2) * pixel_dim_m
    ow = (w / 2) * pixel_dim_m
    pt_imgp_o = np.zeros(pt_imgp.shape)
    pt_imgp_o[0, :] = pt_imgp[0, :] + oh
    pt_imgp_o[1, :] = pt_imgp[1, :] - ow

    # convert to pixels position on img
    pt_img_pixel = np.zeros(pt_imgp_o.shape)
    pt_img_pixel[0, :] = pt_imgp_o[0, :] / pixel_dim_m
    pt_img_pixel[1, :] = pt_imgp_o[1, :] / pixel_dim_m

    return pt_img_pixel.T


# check img pixels which are located in image area
def pixels_in_img(pt_pixels, pointcloud_lidar, w, h):
    pt_pixels = np.around(pt_pixels, decimals=0)
    pt_pixels = pt_pixels.astype(np.int).T
    pt_pixels = -pt_pixels
    pt_img = []
    intensity = []
    for i in range(pt_pixels.shape[1]):
        # if -w <= pt_pixels[0, i] <= w and -w <= pt_pixels[1, i] < w:
        if 0 <= pt_pixels[0, i] <= h and 0 <= pt_pixels[1, i] < w:
            pt_img.append([pt_pixels[0, i], pt_pixels[1, i]])
            intensity.append(pointcloud_lidar[3, i])
    pt_img = np.array(pt_img)
    intensity = np.array(intensity)
    return pt_img, intensity


def gen_color_for_intensity(pt_img, intensity):
    colors = ['r', 'g', 'b', 'y', 'magenta', 'c', 'w']
    step = (np.max(intensity) - np.min(intensity)) / len(colors)
    min = np.min(intensity)
    colors_pt = []
    for i in range(len(colors)):
        colors_pt.append(pt_img[np.logical_and(intensity >= min + (step * i),
                                               intensity <= min + (step * (i + 1))), :])
    return colors_pt, colors


def plot_pt(pt_img, intensity):
    view = np.array(Image.open("./cameraimage.jpeg"))
    colors_pt, colors = gen_color_for_intensity(pt_img, intensity)
    plt.imshow(view)
    # plt.scatter(pt_img[:, 1], pt_img[:, 0], s=8, color='red')
    for i in range(len(colors_pt)):
        plt.scatter(colors_pt[i][:, 1], colors_pt[i][:, 0], s=8, color=colors[i])
    plt.show()
    return


if __name__ == '__main__':
    h = 960
    w = 1280
    # version 1
    pt_pixels = pc_to_pixels(ptc_homo, R, T)
    pt_img, intensity = pixels_in_img(pt_pixels, pointcloud_lidar, w, h)
    print("len img points = ", len(pt_img))
    plot_pt(pt_img, intensity)
    # print("intensity min", np.min(intensity), " max ", np.max(intensity))
    # plt.hist(intensity, bins=10)
    # plt.show()

    # version 2
    pt_pixels = pc_to_pixels_v2(ptc_homo, R, T)
    pt_img, intensity = pixels_in_img(pt_pixels, pointcloud_lidar, w, h)
    print("len img points = ", len(pt_img))
    plot_pt(pt_img, intensity)







